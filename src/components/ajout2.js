import axios from 'axios';
import React from 'react';

/* let deps = [{dep: 'Gestion de réseaux', superviseur: 'Marc Grenier', code: '420.ab'},
            {dep: 'Programmation web', superviseur: 'Simon Delisle', code: '1420.ac'},
            {dep: 'Ajout Sup', superviseur: 'Michel Delisle', code: '2420.an'}
]  */

class Ajout2 extends React.Component{


    constructor(props){
        super(props);

        this.state={    /* declaration des variables dans le get avant le changement! */
            local: '',
            batisse: '',
            dep: '',
            /* deps:[] */
        }

        this.onChangeLocal = this.onChangeLocal.bind(this);
        this.onChangeBatisse = this.onChangeBatisse.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.onChangeDep = this.onChangeDep.bind(this);
    }



/* setters */ 
    onChangeLocal(e){
        this.setState({
            local: e.target.value
        })
    }

    onChangeBatisse(e){
        this.setState({
            batisse: e.target.value
        })
    }

    onChangeDep(e){
        this.setState({
            dep: e.target.value
        })
    }

    onSubmit(e){
        e.preventDefault();
        const util = {
            dep: this.state.dep,
            local: this.state.local,
            batisse: this.state.batisse
            
            
        }
        console.log(util);
        this.setState({
           local: '',
           batisse:''
        })
    }

    componentDidMount(){
        this.setState({
            deps: deps.map(dep=>dep.dep),
            dep: deps[0].dep,
        })
    }

    render(){
        return(
            <div className="container">
            <h3>Réserver un local</h3>

            <form onSubmit={this.onSubmit}>

                <div className="form-group">
                    <label>Départements</label>
                    <select required className="form-control" onChange={this.onChangeDep}>
                    {
                        this.state.deps.map(function(dep){
                            return <option  key = {dep}                    
                                            value = {dep}>{dep}</option>;                   /* On lui demande de faire le lenght du tableau deps avec les 
                                                                                            departements et remplir un option avec chacun */
                        })
                    }
                    </select>
<br></br>
                    <label>Local : </label>
                    <input type="text" required  className="form-control" 
                    value={this.state.local}
                    onChange={this.onChangeLocal}/>  {/* onChange = a un eventListener */}


                    <label>Bâtisse : </label>
                    <input type="text" required  className="form-control" 
                    value={this.state.batisse}
                    onChange={this.onChangeBatisse}/>

                </div>

                <div className="form-group">
                    <input type="submit" value="Ajout" className="btn btn-primary"/>
                </div>
            </form>
        </div>
        )
    }
}
export default Ajout2;