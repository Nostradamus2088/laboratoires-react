import React from 'react';
import axios from 'axios';


class Edit extends React.Component{
    constructor(props){
        super(props);
        this.onChangeCode = this.onChangeCode.bind(this);
        this.onChangeNom = this.onChangeNom.bind(this);
        this.onChangePrenom = this.onChangePrenom.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        
        this.state={  /* Getter */
            code: '',
            nom: '',
            prenom: '',
            id: ''
        }
    }

    componentDidMount(){
        console.log(this.props.match.params.id);

        axios.get('http://10.30.40.121:3877/etudiant/'+ this.props.match.params.id).then(response =>{
            /* console.log(response.data); */
                this.setState({
                code: response.data.data.code,
                nom: response.data.data.nom,
                prenom: response.data.data.prenom,
                id: this.props.match.params.id
            })
        })
        .catch((error)=>{
            console.log(error);
        })
        console.log(this.state.code);
    }

    /* setters */
    onChangeCode(e){
        this.setState({
            code: e.target.value
        })
    }

    onChangeNom(e){
        this.setState({
            nom: e.target.value
        })
    }

    onChangePrenom(e){
        this.setState({
            prenom: e.target.value
        })
    }

    onSubmit(e){
        e.preventDefault();
        let util = {
            code: this.state.code,
            nom: this.state.nom,
            prenom: this.state.prenom
        }
        console.log(util);
        axios.put('http://10.30.40.121:3877/etudiant/'+ this.props.match.params.id, util) //dois absolument passer une parametre lors du post car il doit faire laction. 
        .then(res=>console.log(res.data));
        this.setState({
            prenom: '',
            nom: '',
            code: ''
        })
    }


    render(){
        return(
            <div className="container">
                <h3>Ajouter un utilisateurs</h3>

                <form onSubmit={this.onSubmit}>

                    <div className="form-group">
                        <label>Nom : </label>
                        <input type="text" required  className="form-control" 
                        value={this.state.nom}
                        onChange={this.onChangeNom}/>  {/* onChange = a un eventListener */}


                        <label>Prenom : </label>
                        <input type="text" required  className="form-control" 
                        value={this.state.prenom}
                        onChange={this.onChangePrenom}/>


                        <label>Code : </label>
                        <input type="text" required  className="form-control" 
                        value={this.state.code}
                        onChange={this.onChangeCode}/>
                    </div>

                    <div className="form-group">
                        <input type="submit" value="Edit" className="btn btn-primary"/>
                    </div>
                </form>
            </div>
        )
    }
    
}
export default Edit;