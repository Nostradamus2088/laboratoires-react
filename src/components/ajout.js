import axios from 'axios';
import React from 'react';

class Ajout extends React.Component{
    constructor(props){
        super(props);
        this.state={  /* Getter */
            dep:'',
            code: '',
            nom: '',
            prenom: '',
            deps:[]
        }
/*             dans le contrusteur faire sur quil es access au setters et quil relit le changement
 */        
        this.onChangeCode = this.onChangeCode.bind(this);
        this.onChangeNom = this.onChangeNom.bind(this);
        this.onChangePrenom = this.onChangePrenom.bind(this);
        this.onChangeDepartement = this.onChangeDepartement.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        
        
    }

    componentDidMount(){
        // axios.get('http://localhost:3026/lireDep')

        axios.get('http://10.30.40.121:3877/departement')
        .then(response => {
            console.log(response.data.data);
            if (response.data.data.length > 0){
                this.setState({
                    deps: response.data.data.map(dep=> dep.dep),
                    dep:response.data.data[0].dep
             })
             console.log(response.data.data);
        }
    })
    .catch((error)=>{
        console.log(error);
    })
}

    

    /* setters */
    onChangeCode(e){
        this.setState({
            code: e.target.value
        })
    }

    onChangeNom(e){
        this.setState({
            nom: e.target.value
        })
    }

    onChangePrenom(e){
        this.setState({
            prenom: e.target.value
        })
    }

    onChangeDepartement(e){
        this.setState({
            dep:e.target.value
        })
    }


    onSubmit(e){
        e.preventDefault();
        const util = {
            code: this.state.code,
            nom: this.state.nom,
            prenom: this.state.prenom,
            dep: this.state.dep
        }
        console.log(util);
        axios.post('http://10.30.40.121:3877/etudiant', util) //dois absolument passer une parametre lors du post car il doit faire laction. 
        .then(res=>console.log(res.data));
        this.setState({
            prenom: '',
            nom: '',
            code: '', 
            dep:''
        })
    }


    render(){
        return(
            <div className="container">
                <h3>Ajouter un utilisateurs</h3>

                <form onSubmit={this.onSubmit}>
                <div className="form-group">
                       <label>Départements :  </label>
                       <select
                        
                        className="form-control"
                            onChange={this.onChangeDepartement}>
                            {
                                this.state.deps.map(function(dep){
                                    return <option
                                    key={dep}
                                    value={dep}>{dep}
                                    </option>
                                })
                            }
                        </select>
                   </div>
                    <div className="form-group">
                        <label>Nom : </label>
                        <input type="text" required  className="form-control" 
                        value={this.state.nom}
                        onChange={this.onChangeNom}/>  {/* onChange = a un eventListener */}


                        <label>Prenom : </label>
                        <input type="text" required  className="form-control" 
                        value={this.state.prenom}
                        onChange={this.onChangePrenom}/>


                        <label>Code : </label>
                        <input type="text" required  className="form-control" 
                        value={this.state.code}
                        onChange={this.onChangeCode}/>
                    </div>

                    <div className="form-group">
                        <input type="submit" value="Ajout" className="btn btn-primary"/>
                    </div>
                </form>
            </div>
        )
    }
    
}
export default Ajout;