import React from 'react';
import {BrowserRouter as Router, Route} from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css"
import Navbar from './components/navbar';
import Liste from './components/liste';
import Ajout from './components/ajout';
import Edit from './components/edit/edit';
import Logo from './components/logo';
import AjoutDep from './components/ajoutDep';


class App extends React.Component {
  render(){
    return(
      
      <div className="container">
       
        <Router>
          <Navbar/>
          <Logo/>
          <Route path="/liste" component={Liste}/>
          <Route path="/ajout" component={Ajout}/>
          <Route path="/ajoutDep" component={AjoutDep}/>
          <Route path="/edit/:id" component={Edit}/>
    
        </Router>

         {/* affiche le components logo */}


    

      </div>
    )
  }
}
export default App;